@extends('layout')

@section('title', 'Projects')

@section('content')

    <h1 class="title">Projects</h1>

    <ul>
        @foreach($projects as $project)

            <li>
                <a href="/projects/{{ $project->id }}">{{ $project->title }}</a>
            </li>

        @endforeach
    </ul>

    <p>
        <a class="button" href="/projects/create">Create Project</a>
    </p>

@endsection
